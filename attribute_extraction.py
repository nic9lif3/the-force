import pandas as pd
from urllib.parse import urlparse, unquote, quote
from random import shuffle
from tqdm import tqdm
import numpy as np
import time
import re
import json


def decode(payload):
    n=0
    while True:
        pre_payload = payload
        payload = unquote(payload)
        if n==0:
            payload1=payload
        if pre_payload == payload:
            break
        n+=1
    return n,payload1,payload

def check_smaller_than_32(payload):
    tmp=np.repeat(np.expand_dims(payload,0),32,0)
    tmp1=np.expand_dims(np.arange(32),1)
    return (np.sum(tmp==tmp1,axis=1)).astype(int)

def count_number_another(payload):
    sets=[['../','..\\'],
        ['--','/*','*/','||','&&','>>','/>','<?','?>'],
        ['#', ',', ';', '`', '$', '#', '!', '-', '?', '=', '[', ']', '~' ,'.', '%', ':', '+', '(', ')', '{', '}', '<', '>', '|', '/', '\\', '&', '*','\'','"']]
    l=len(sets)
    sets_= [[0]*len(i) for i in sets]

    for i in range(len(sets)):
        for j in range(len(sets[i])):
            tmp=payload.replace(sets[i][j],'')
            sets_[i][j]=int((len(payload)-len(tmp))/(l-i))
            payload=tmp
    result=[]
    for i in sets_:
        result+=i
    return result,payload

def check_exist_keyword(payload):
    key_word=['create','drop','alter','truncate',
        'select', 'insert', 'update', 'delete',
        'or', 'and', 'union','join','where','if',
        'convert','cast','hextoraw',
        'cat','ls','mv','cp','rm',
        'perl','python','ruby','lua',
        'node', 'xml', 'encoding', 'cdata', 'doctype', 'element',
        'onload', 'onmousehover', 'onerror',
        'script','iframe', 'var', 'alert',
        'lt','gt']
    comment=[['[/][*]','[*][/]'],['<!--','-->']]
    for i in comment:
        payload=re.sub(i[0]+'(.*?)'+i[1],'',payload)
    payload=payload.lower()

    key=[0]*len(key_word)

    for i in range(len(key_word)):
        key[i]=int((len(payload)-len(payload.replace(key_word[i],'')))/len(key_word[i]))
    
    return key

def check_raito(value):
    if isinstance(value,(list,)):
        value=''.join(value)
    if len(value)==0:
        return [0,0,0,0]
    n1=sum([i.isalpha() for i in value])/len(value)
    n2=sum([i.isdigit() for i in value])/len(value)
    return [len(value),n1,n2,1-n1-n2]

def process_method(method):
    m=['OPTIONS','GET','HEAD','POST','PUT','DELETE','TRACE','CONNECT']
    return (np.array(m)==method).astype(np.int)

def process_type(normal_type,request_type):
    return (np.array(normal_type)==request_type).astype(np.int)

def process_subtype(normal_subtype,request_subtype):
    return (np.array(normal_subtype)==request_subtype).astype(np.int)

def get_keys(data):
    keys=[]
    for i in data:
        keys+=json.loads(i).keys()
    return list(set(keys))

def get_types(data):
    types=list(set([i.split('/')[0] for i in data.unique() if len(i.split('/'))==2]))
    return types

def get_subtypes(data):
    subtypes=list(set([i.split('/')[1] for i in data.unique() if len(i.split('/'))==2]))
    return subtypes

def process_payload(payload):
    n,payload1,payloadn=decode(quote(payload))
    q_payload=np.array([ord(i) for i in payload1])
    s2=check_smaller_than_32(q_payload).tolist()
    s3_1,_=count_number_another(payload1)
    s4=check_raito(payload1)
    if n>1:
        s3_2,_=count_number_another(payloadn)
    else:
        s3_2=s3_1[::1]
    
    s1=check_exist_keyword(payload)

    return s1+s2+s3_1+s3_2+s4

def process_content_type(types,subtypes,data):
    s1=process_method(data['Method'])
    if len(data['Content_Type'].split('/'))==2:
        request_type,request_subtype=data['Content_Type'].split('/')
    else:
        request_type,request_subtype='',''
    s2=process_type(types,request_type)
    s3=process_subtype(subtypes,request_subtype)
    return list(s1+s2+s3)

def process_structed_data(keys,data):
    s5=[]
    keys=np.array(keys)
    try:
        data=json.loads(data)
    except:
        data={}
    s5.append(len(data.keys())-np.sum([keys==i for i in data.keys()]))
    for key in keys:
        if key not in data.keys():
            s5+=[0,0,0,0]
        else:
            s5+=check_raito(data[key])
    return s5

# import pandas as pd
# data1=pd.read_excel('csic_2010/anomalousTrafficTest.xlsx')
# data1=data1.replace(pd.np.nan,'',regex=True)
# keys=get_keys(data1.Structed_data)
# types=get_types(data1.Content_Type)
# subtypes=get_subtypes(data1.Content_Type)
# data1=data1.iloc[0]

# t=time.time()
# a=process_structed_data(keys,data1.Structed_data)
# b=process_header(types,subtypes,data1)
# c=process_payload(data1.Payload)
# print(time.time()-t)