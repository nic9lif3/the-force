from urllib.parse import urlparse
import pandas as pd
import json
from glob import glob
def convert(file_text,file_csv):
    with open(file_text,'r') as f:
        data=f.read()

    fields=['Method','Url','Protocol','User_Agent','Pragma','Cache_control','Accept','Accept_Encoding','Accept_Charset','Accept_Language','Host','Connection','Content_Length','Content_Type','Cookie','Structed_data','Payload']
    m=['OPTIONS','GET','HEAD','POST','PUT','DELETE','TRACE','CONNECT']
    extract={}
    [extract.update({i:[]}) for i in fields]

    pos=-1 
    pre_pos=0
    l=len(data)
    field_exist={}

    while True:
        [field_exist.update({i:False}) for i in fields]
        print(pre_pos,'/',l,end='\r')
        if pre_pos==l:
            break

        pos_method=[data.find(i+' ',pre_pos+1) for i in m]
        pos_method=[i if i!=-1 else l for i in pos_method]
        
        pos=min(pos_method)

        content=[i for i in data[pre_pos:pos].split('\n') if len(i)>0]
        
        method,query,protocol=content[0].split(' ')
        extract['Method'].append(method)
        field_exist['Method']=True

        tmp=urlparse(query)

        extract['Url'].append(tmp.netloc+tmp.path)
        field_exist['Url']=True
        extract['Protocol'].append(protocol)
        field_exist['Protocol']=True

        if method=='GET':
            extract['Payload'].append(tmp.query)          
            field_exist['Payload']=True
            if len(tmp.query)>0:
                keyvalue=tmp.query.split('&')
                dicttmp={}
                for i in keyvalue:
                    if len(i.split('='))==1:
                        dicttmp.update({i:''})
                    elif len(i.split('='))==2:
                        dicttmp.update({i.split('=')[0]:i.split('=')[1]})
                keyvalue=dicttmp
            else:
                keyvalue={}
            extract['Structed_data'].append(json.dumps(keyvalue))          
            field_exist['Structed_data']=True
            for i in content[1:]:
                find=i.find(':')
                extract[i[:find]].append(i[find+1:].strip())
                field_exist[i[:find]]=True
        else:
            extract['Payload'].append(content[-1])          
            field_exist['Payload']=True
            if len(content[-1])>0:
                keyvalue=content[-1].split('&')
                for i in keyvalue:
                    if len(i.split('='))==1:
                        dicttmp.update({i:''})
                    elif len(i.split('='))==2:
                        dicttmp.update({i.split('=')[0]:i.split('=')[1]})
                keyvalue=dicttmp
            else:
                keyvalue={}
            extract['Structed_data'].append(json.dumps(keyvalue))          
            field_exist['Structed_data']=True
            for i in content[1:-1]:
                find=i.find(':')
                extract[i[:find]].append(i[find+1:].strip())
                field_exist[i[:find]]=True
            
        for key in field_exist.keys():
            if not field_exist[key]:
                extract[key].append('')

        pre_pos=pos

    csv=pd.DataFrame()
    for i in fields:
        csv[i]=extract[i]
    csv.to_excel(file_csv,index=False)

for i in glob('csic_2010/*txt'):
    print(i)
    convert(i,i.replace('txt','xlsx'))

        