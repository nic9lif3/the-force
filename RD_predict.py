from attribute_extraction import *
from joblib import load
import numpy as np
import pandas as pd           

class RDFR_predict:
    def __init__(self):
        self.des=load('des.joblib')
        self.rd=load('model1.joblib')
        FIELD_NAMES = ["method", "length_path",
               "printable_characters_ratio_path", "non_printable_characters_ratio_path", "letter_ratio_path",
               "digit_ratio_path", "num_segment", "is_file", "file_extension", "num_parameters",
               "length_query", "printable_characters_ratio_query", "non_printable_characters_ratio_query",
               "letter_ratio_query", "digit_ratio_query", "num_headers",
               "standard_headers_ratio", "non_standard_headers_ratio"]
        for h in STANDARD_HEADERS:
            FIELD_NAMES.append("length_header_" + h)
        for h in STANDARD_HEADERS:
            FIELD_NAMES.append("printable_characters_ratio_header_" + h)
        for h in STANDARD_HEADERS:
            FIELD_NAMES.append("non_printable_characters_ratio_header_" + h)
        for h in STANDARD_HEADERS:
            FIELD_NAMES.append("letter_ratio_header_" + h)
        for h in STANDARD_HEADERS:
            FIELD_NAMES.append("digit_ratio_header_" + h)
        for h in STANDARD_HEADERS:
            FIELD_NAMES.append("is_standard_header_" + h)
        FIELD_NAMES += ["is_persistent_connection", "content_type", "length_body", "printable_characters_ratio_body",
                        "non_printable_characters_ratio_body", "letter_ratio_body", "digit_ratio_body"]
        self.index=FIELD_NAMES
        self.accept_key=np.load('accept_key.npy')   

    def process(self,logs):
        requests=[]
        for block in logs:
            request = normalize_request(block)
            requests.append(method(request) + length_path(request) + printable_characters_ratio_path(request) + non_printable_characters_ratio_path(request) + letter_ratio_path(request) + digit_ratio_path(request) + num_segment(request) + file_extension(request) + num_parameters(request) + length_query(request) + printable_characters_ratio_query(request) + non_printable_characters_ratio_query(request) + letter_ratio_query(request) + digit_ratio_query(request) + num_headers(request) + standard_headers_ratio(request) + non_standard_headers_ratio(request)+ length_header(request) + printable_characters_ratio_header(request) + non_printable_characters_ratio_header(request) + letter_ratio_header(request) + digit_ratio_header(request)+ is_standard_header(request) + is_persistent_connection(request) + content_type(request) + length_body(request) + printable_characters_ratio_body(request) + non_printable_characters_ratio_body(request) + letter_ratio_body(request) + digit_ratio_body(request))
    
        data=pd.DataFrame(requests,columns=self.index)
        
        preprocess_data=[]
        for key in tqdm(self.accept_key):
            if data[key].dtype=='int64' or data[key].dtype=='float64':
                nm=(data[key]-self.des[key][0])/self.des[key][1]
                nm=np.expand_dims(nm.tolist(),0).T
            elif data[key].dtype=='O':
                nm=np.zeros((len(data[key]),len(self.des[key].keys())))
                for i,n in enumerate(data[key]):
                    try:
                        nm[i,self.des[key][n]]=1
                    except:
                        continue  
            else:
                continue
            preprocess_data.append(nm) 
        preprocess_data=np.concatenate(preprocess_data,axis=1)
        return self.rd.predict(preprocess_data)>=0.5


predict=RDFR_predict()


data=read_file_text('anomalous_test.txt')


a = predict.process(data)

print(sum(a))


