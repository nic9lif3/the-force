import pandas as pd
import numpy as np
from sklearn.svm import SVC
from sklearn.metrics import classification_report,confusion_matrix
from tqdm import tqdm
from sklearn.ensemble import RandomForestClassifier
from joblib import dump, load
from attribute_extraction import *
import os 
from glob import glob

def train_RF(folder):
    try:
        if os.path.exists('info_rf.json'):
            with open('info_rf.json','r') as f:
                info=json.load(f)
        else:
            info={}
    except Exception:
        info={}
    if not os.path.exists('rf_model'):
        os.makedirs('rf_model')
    
    for path in glob(folder+'/*xlsx'):
        data=pd.read_excel(path).fillna('')
        data=data.sample(frac=1).reset_index(drop=True)
        split=int(0.8*len(data))
        train_data=data.iloc[:split]
        test_data=data.iloc[split:]
        label_train=train_data['human_label']
        label_test=test_data['human_label']

        keys=get_keys(data[data['human_label']==0].Structed_data)
        types=get_types(data[data['human_label']==0].Content_Type)
        subtypes=get_subtypes(data[data['human_label']==0].Content_Type)
        
        path_id=path.split('/')[-1][:-5]

        if not os.path.exists('normal_types.json'):
            normal_types_file={}
        else:
            with open('normal_types.json','r') as f:
                normal_types_file=json.load(f)
        
        normal_types_file[path_id]=types
        with open('normal_types.json','w') as f:
            json.dump(normal_types_file,f)  
                
                
        if not os.path.exists('normal_subtypes.json'):
            normal_subtypes_file={}
        else:
            with open('normal_subtypes.json','r') as f:
                normal_subtypes_file=json.load(f)
        
        normal_subtypes_file=subtypes
        with open('normal_subtypes.json','w') as f:
            json.dump(normal_subtypes_file,f)  
            

        if not os.path.exists('normal_keys.json'):
            normal_keys_file={}
        else:
            with open('normal_keys.json','r') as f:
                normal_keys_file=json.load(f)
        normal_keys_file=keys
        with open('normal_keys.json','w') as f:
            json.dump(normal_keys_file,f)         
        
        
        data_train=[]
        data_test=[]
        for _,row in train_data.iterrows():
            data_train.append(process_payload(row.Payload)+\
                              process_content_type(types,subtypes,row)+\
                              process_structed_data(keys,row.Structed_data))
        for _,row in test_data.iterrows():
            data_test.append(process_payload(row.Payload)+\
                              process_content_type(types,subtypes,row)+\
                              process_structed_data(keys,row.Structed_data))       
        data_train=np.array(data_train)
        data_test=np.array(data_test)    


        rf = RandomForestClassifier(n_estimators = 300,n_jobs=-1,class_weight={0:7})
        rf.fit(data_train,label_train)
        pred=rf.predict(data_test)

        tn, fp, fn, tp=confusion_matrix(label_test,pred).ravel()



        if path_id in info:
            verson=max([int(i) for i in info[path_id].keys()])
            verson=verson+1
        else:
            info[path_id]={}
            verson=1
        if not os.path.exists('{}/{}'.format('rf_model',path_id)):
            os.makedirs('{}/{}'.format('rf_model',path_id))
        model_file='{}/{}/verson_{}.joblib'.format('rf_model',path_id,verson)
        print(path_id,(tp+tn)/(tp+tn+fp+fn))
        dump(rf,model_file)
        info[path_id].update({str(verson):{'path':model_file,'acc':str((tp+tn)/(tp+tn+fp+fn))}})
    with open('info_rf.json','w') as f:
        json.dump(info,f)

train_RF('test')