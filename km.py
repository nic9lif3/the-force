import numpy as np
import os
import tensorflow as tf
import pandas as pd
from glob import glob
from tqdm import tqdm
from attribute_extraction import *

class Kmean:
    def __init__(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        self.sess = tf.Session(config=config) 

    def read_data(self,csv_file,num_cols,batch_size):
        dataset = tf.data.experimental.CsvDataset(csv_file,[tf.float32]*num_cols).batch(batch_size//num_cols)
        return dataset
    
    def inference(self,inp, num_clusters):

        kmeans = tf.contrib.factorization.KMeans(
            inputs=inp,
            num_clusters=num_clusters,
            distance_metric=tf.contrib.factorization.COSINE_DISTANCE,
            use_mini_batch=True,
            mini_batch_steps_per_iteration= self.num_batch
        )

        (_, clustering_indexs,scores , _, kmeans_init, kmeans_training_op) = kmeans.training_graph()

        return clustering_indexs, kmeans_init, kmeans_training_op,scores
    
    def training_with_n(self,csv_file,num_cols,n,threshhold=None):        
    
        clustering_indexs, kmeans_init, kmeans_training_op, scores = self.inference(self.data_train,n)

        self.sess.run(tf.global_variables_initializer())
        
        self.sess.run(self.iters.initializer)
        
        self.sess.run(kmeans_init)
        
        index=[]
        
        self.sess.run(self.iters.initializer)
        try:
            while True:
                index.append(self.sess.run(clustering_indexs)[0])
        except tf.errors.OutOfRangeError:
            pass
        
        index=np.concatenate(index)
        l=len(index)
        step=0
        
        while True:
            self.sess.run(self.iters.initializer)
            try:
                while True:
                    self.sess.run(kmeans_training_op)
            except tf.errors.OutOfRangeError:
                pass
        
            tmp=np.array(index)
            index=[]
            self.sess.run(self.iters.initializer)
            score=0
            try:
                while True:
                    tmp1,tmp2=self.sess.run([clustering_indexs,scores])
                    index.append(tmp1[0])
                    score+=np.sum(tmp2[0]**2)
            except tf.errors.OutOfRangeError:
                pass
            
            index=np.concatenate(index)
            step+=1
            diff=sum(index!=tmp)
            if step>=3:
                if step==30 or diff==0:
                    break
                if diff/l*100<0.1:
                    if threshhold:
                        if score<threshhold:
                            break
                    else:
                        break

                
        return index,score
    
    def run_training(self,csv_file,num_cols,batch_size=5000000):

        self.dataset=self.read_data(csv_file,num_cols,batch_size)

        self.iters=self.dataset.make_initializable_iterator()

        self.data_train=tf.transpose(self.iters.get_next())
        
        self.num_batch=0
        try:
            self.sess.run(self.iters.initializer)
            while True:
                self.sess.run(self.data_train)
                self.num_batch+=1
        except tf.errors.OutOfRangeError:
            pass
        
        indexes=[]
        wws=[]
        index,score=self.training_with_n(csv_file,num_cols,1)
        indexes.append(index)
        wws.append(score)
        for i in range(2,11):
            index,score=self.training_with_n(csv_file,num_cols,i,wws[-1])
            wws.append(score)
            indexes.append(index)
        tmp=[abs(wws[i+1]-wws[i]) for i in range(9)]
        tmp=[tmp[i+1]/tmp[i] for i in range(8)]
        return indexes,np.argmin(tmp)+2,wws
        
    
    def close(self):
        self.sess.close()

def km_folder(folder):
    km=Kmean()
    for file in glob(folder+'/*xlsx'):
        print(file)
        csv=pd.read_excel(file).fillna('')
        data=[]
        keys=get_keys(csv.Structed_data)
        types=get_types(csv.Content_Type)
        subtypes=get_subtypes(csv.Content_Type)
        for i,row in csv.iterrows():
            data.append(process_payload(row.Payload)+\
                              process_header(types,subtypes,row)+\
                              process_structed_data(keys,row.Structed_data))
        pd.DataFrame(data).to_csv('tmp.csv',index=False,header=False)
        result,index,_=km.run_training('tmp.csv',len(data[0]))
        csv['KM_label']=result[index-1]
        csv.to_excel(file)
